package preda.dragos.lab2.ex6;
import java.util.Scanner;
public class Factorial {
    public static int factorial(int n)
    {
        if(n == 1)
            return 1;
        else
            return n*factorial(n-1);
    }
    public static void main(String[] args)
    {
        System.out.print("Enter a number: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int value = 1;
        for(int i = n; i >= 1; i--)
        {
            value = value * i;
        }
        System.out.print("\nValue obtained through Non-recursive method: " + value +"\n");
        value = factorial(n);
        System.out.print("Value obtained through Rrecursive method: " + value +"\n");
    }
}
