package preda.dragos.lab2.ex5;

public class Bubble_Sort {
    public static void main(String[] args){
        int[] array = new int[10];
        int n = array.length, aux;
        System.out.print("The unsorted array is: ");
        for(int i = 0; i < n; i++)
        {
            array[i] = (int)(Math.random()*100);
            System.out.print(array[i] + " ");
        }
        for(int i = 0; i < n-1; i++)
        {
            for(int j = i+1; j < n; j++)
            {
                if(array[i] > array[j])
                {
                    aux = array[i];
                    array[i]=array[j];
                    array[j]=aux;
                }
            }
        }
        System.out.print("\n The sorted array is:");
        for(int i = 0; i < n; i++)
        {
            System.out.print(array[i] + " ");
        }
    }
}
