package preda.dragos.lab2.ex4;
import java.util.Scanner;
public class Max {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n, a[], max = -10000;
        a = new int[100];
        System.out.print("Introduce the number of elements and then the elements:\n");
        n = in.nextInt();
        for(int i = 1; i <= n; i++ ) {
            a[i] = in.nextInt();
            if(a[i] > max)
                max = a[i];
            }
        System.out.print("The max number is: ");
        System.out.print(max);
        }
    }

