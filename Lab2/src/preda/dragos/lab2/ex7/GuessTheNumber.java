package preda.dragos.lab2.ex7;
import java.util.Scanner;
public class GuessTheNumber {
    public static void main(String[] args){
        int n = (int)(Math.random()*100);
        int counter = 0;
        int number = 0;
        while(true){
            if(n > 20)
                n = (int)(Math.random()*100);
            else
                break;
        }
        System.out.print("Try to guess the number. Please enter a number between 1-20:\n");
        while(true){
            Scanner scanner = new Scanner(System.in);
            number = scanner.nextInt();
            counter++;
            if(number == n)
                { System.out.print("Good job!!! You have guessed the number! "); break; }
            else
                if(counter == 3)
                { System.out.print("You lost!!!.....The number was: "+n); break; }
                else
                    if(number > n)
                        System.out.print("Wrong answer, your number is too high\n");
                    else
                        System.out.print("Wrong answer, your number is too low\n");
        }
    }
}
