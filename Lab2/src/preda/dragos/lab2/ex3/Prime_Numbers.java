package preda.dragos.lab2.ex3;
import java.util.Scanner;
public class Prime_Numbers {
    public static void main(String[] args){
        System.out.print("Define the interval...Enter 2 numbers\n");
        Scanner in = new Scanner(System.in);
        int prim = 0,ok = 1;
        int a = in.nextInt();
        int b = in.nextInt();
        for( int i=a; i<=b; i++ )
        {
            ok = 1;
            for( int j=2; j<=Math.sqrt(i); j++)
            {
                if(i % j == 0)
                { ok = 0; break;}
            }
            if(ok == 1)
            {
                System.out.print(i);
                System.out.print(" ");
                prim++;
            }

        }
        System.out.print("\nThere are ");  System.out.print(prim);  System.out.print(" prime numbers\n");

    }
}
