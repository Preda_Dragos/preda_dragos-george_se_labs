package preda.dragos.lab7.ex4;

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws Exception {
        File file = new File("Cars");
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory \"Cars\" is created!");
            } else {
                System.out.println("Failed to create directory \"Cars\"!");
            }
        }
        CarOperations carOp = new CarOperations();
        Car newCar = null;
        char choice;
        String line;
        Scanner in = new Scanner(System.in);
        int index = 0;
        do {
            System.out.println("\nMenu");
            System.out.println("c - Create car");
            System.out.println("s - Save car");
            System.out.println("v - View all cars");
            System.out.println("r - Read a car and display its details");
            System.out.println("e - Exit");

            line = in.next();
            choice = line.charAt(0);

            switch (choice) {
                case 'c':
                case 'C':
                    System.out.print("Introduce model: ");
                    String model = in.next();
                    System.out.print("Introduce price: ");
                    double price = in.nextDouble();
                    newCar = carOp.createCar(model, price);
                    System.out.println("New car created: " + newCar);
                    break;
                case 's':
                case 'S':
                    index++;
                    String path = "Cars\\Car" + index + ".txt";
                    carOp.saveCar(newCar, path);
                    break;
                case 'v':
                case 'V':
                    ArrayList<Car> cars = new ArrayList<>();
                    for (int i = 1; i <= index; i++) {
                        path = "Cars\\Car" + i + ".txt";
                        cars.add(carOp.viewCar(path));
                    }
                    System.out.println("All cars are: ");
                    for (Car car : cars)
                        System.out.println(car);
                    break;
                case 'r':
                case 'R':
                    System.out.print("Introduce the number of the car to be displayed: ");
                    int nr = in.nextInt();
                    if (nr >= 1 && nr <= index) {
                        path = "Cars\\Car" + nr + ".txt";
                        Car chosenCar = carOp.viewCar(path);
                        System.out.println("The details of the chosen car are: " + chosenCar);
                    } else System.out.println("Invalid number!");
                    break;
                default:
                    if (choice != 'e' && choice != 'E')
                        System.out.println("Invalid option chosen!");
            }
        } while (choice != 'e' && choice != 'E');
        System.out.println("Program finished.");
        try{
            File[] files = file.listFiles();
            if (files != null && files.length > 0) {
                for (File aFile : files) {
                    aFile.delete();
                }
            }
            if(file.delete())
                System.out.println("Directory \"Cars\" deleted successfully!");
            else System.out.println("Directory \"Cars\" not deleted!");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}