package preda.dragos.lab7.ex4;

import java.io.*;

public class CarOperations {
    Car createCar(String model, Double price){
        return new Car(model, price);
    }
    void saveCar(Car car, String fileName) throws IOException {
        ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileName));
        o.writeObject(car);
        o.close();
        System.out.println("Car saved successfully");
    }
    Car viewCar(String fileName) throws IOException, ClassNotFoundException{
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
        Car car = (Car)in.readObject();
        in.close();
        return car;
    }
}
