package preda.dragos.lab7.ex1;

class CoffeeMaker {
    private int nr = 0;

    Coffee makeCoffee() throws NrCoffeesException {
        System.out.println("Make a coffee");
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        Coffee coffee = new Coffee(t, c);
        nr++;
        if (nr > 10)
            throw new NrCoffeesException(this.nr, "The number of predefined coffees was exceeded!");
        return coffee;
    }

    public int getNr() {
        return nr;
    }
}
