package preda.dragos.lab7.ex1;

public class NrCoffeesException extends Exception{
    int nr;
    public NrCoffeesException(int nr, String msg){
        super(msg);
        this.nr = nr;
    }
    int getNr(){
        return nr;
    }
}
