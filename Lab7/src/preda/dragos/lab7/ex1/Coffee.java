package preda.dragos.lab7.ex1;

class Coffee {
    private int temp;
    private int conc;
    //private int nr;

    Coffee(int t, int c) {
        temp = t;
        conc = c;
       // nr++;
    }

    int getTemp(){
        return temp;
    }
    int getConc(){
        return conc;
    }
    //int getNr() { return nr; }
    public String toString(){
        return "[coffee temperature="+temp+":concentration="+conc+"]";
    }
}
