package preda.dragos.lab7.ex1;

public class CoffeeTest {
    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for (int i=0;i<11;i++) {
            try {
                Coffee c = mk.makeCoffee();

                try {
                    d.drinkCoffee(c);
                } catch (TemperatureException e) {
                    System.out.println("Exception: " + e.getMessage() + " temp=" + e.getTemp());
                } catch (ConcentrationException e) {
                    System.out.println("Exception: " + e.getMessage() + " conc=" + e.getConc());
                } finally {
                    System.out.println("Throw the coffee cup.\n");
                }

            } catch (NrCoffeesException e) {
                System.out.println("Exception: " + e.getMessage() + " nrCoffee=" + e.getNr());
            }
        }
    }
}
