package preda.dragos.lab7.ex1;

class CoffeeDrinker {
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(), "Coffee is too hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(), "Coffee's concentration is too high!");
//        if(c.getNr()>10)
//            throw new NrCoffeesException(c.getNr(), "The number of predefined coffees was exceeded!");
        System.out.println("Drink coffee: "+c);
    }
}
