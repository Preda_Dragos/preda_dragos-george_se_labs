package preda.dragos.lab7.ex2;

import java.io.*;

public class Main {
    static int countChar(char c, String s) {
        int times = 0;
        char[] a = s.toCharArray();
        for (int i = 0; i < a.length; i++) {
            if (a[i] == c)
                times++;
        }
        return times;
    }

    public static void main(String[] args) {

        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Data.txt")));
            out.print("Ana are 10 mere, dar jumatate sunt stricate.");
            out.close();

            BufferedReader in = new BufferedReader(new FileReader("Data.txt"));
            String s = in.readLine();
            System.out.println(s);
            try {
                BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
                    System.out.print(">>Select character: ");
                    char character = fluxIn.readLine().charAt(0);
                    System.out.println("Chosen character appears " + countChar(character, s)+" times in the file");
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("Eroare :" + e.getMessage());

            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

}
