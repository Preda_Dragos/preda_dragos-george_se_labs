package preda.dragos.lab4.ex4;

import preda.dragos.lab4.ex2.Author;

import java.util.Scanner;

public class test {
    public static void main(String args[]){
        boolean ok=true;
        String name,email,bookname;
        char input;
        int price,qtyInStock,counter;
        char gender;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Add a Book: ");
        bookname = scanner.next();
        Author[] author = new Author[100];
        while(ok) {
            System.out.print("Add its Author: Name: ");
            scanner.nextLine();
            name = scanner.nextLine();
            System.out.print(" Gender: ");
            gender = scanner.next().charAt(0);
            System.out.print(" Email : ");
            email = scanner.next();
            counter = Book.getCounter();
            author[counter] = new Author(name,email,gender);
            Book.setCounter(++counter);
            System.out.print(" Do you want to add another author? Y/N\n");
            while( true ) {
                input = scanner.next().charAt(0);
                if (input == 'N' || input == 'n') {
                    ok = false;
                    break;
                }
                else {
                    if (input != 'Y' && input != 'y')
                        System.out.print(" Wrong input. Try again\n");
                    else
                        break;
                }
            }
        }
        System.out.print("Add its price($): ");
        price = scanner.nextInt();
        System.out.print("Available quantity in stock ");
        qtyInStock = scanner.nextInt();
        Book book = new Book(bookname,author,price,qtyInStock);
        System.out.print(book.toString());
        book.printAuthors();
    }
}
