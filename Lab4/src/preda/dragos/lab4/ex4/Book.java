package preda.dragos.lab4.ex4;
import preda.dragos.lab4.ex2.Author;
public class Book {
        private String name;
        private Author[] authors;
        private double price;
        private int qtyInStock = 0;
        private static int  counter;

    public Book(String name, Author[] authors, double price){
            this.name = name;
            this.authors = authors;
            this.price = price;
        }
        public Book(String name, Author[] authors, double price, int qtyInStock){
            this.name = name;
            this.authors = authors;
            this.price = price;
            this.qtyInStock = qtyInStock;
        }

        public String getName() {
            return name;
        }

        public Author[] getAuthors() {
            return authors;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getQtyInStock() {
        return qtyInStock;
        }

        public static int getCounter(){
        return counter;
        }

        public static void setCounter(int counter) {
            Book.counter = counter;
        }

        public void setQtyInStock(int qtyInStock) {
            this.qtyInStock = qtyInStock;
        }
        @Override
        public String toString(){
            String s = name + " by " + counter + " authors " +"\n";
            return s;
        }
        public void printAuthors(){
            for(int i=0 ; i<counter; i++)
                System.out.print("Author no." + (i+1) + " " + authors[i] + "\n");
        }
    }


