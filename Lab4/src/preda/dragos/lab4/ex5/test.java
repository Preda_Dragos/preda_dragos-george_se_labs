package preda.dragos.lab4.ex5;

public class test {
    public static void main(String[] args){
   Circle circle1 = new Circle();
   Circle circle2 = new Circle(10);
   Cylinder cylinder1 = new Cylinder();
   Cylinder cylinder2 = new Cylinder(5);
   Cylinder cylinder3 = new Cylinder(5,10);
   System.out.print("First Circle: " + circle1.toString() + "\n");
    System.out.print("Second Circle: " + circle2.toString() + "\n");
    System.out.print("First Cylinder: " + cylinder1.toString() + "\n");
    System.out.print("Second Cylinder: " + cylinder2.toString() + "\n");
    System.out.print("Third Cylinder: " + cylinder3.toString() + "\n");

    }
}
