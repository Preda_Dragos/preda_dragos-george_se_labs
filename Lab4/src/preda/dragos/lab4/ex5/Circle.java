package preda.dragos.lab4.ex5;

public class Circle {
    private double radius = 1.0 ;
    private String color = "red";

    public Circle(){
    }

    public Circle(double radius){
       this.radius = radius;
    }
    public double getRadius(){
        return this.radius;
    }
    public String getColor() {return this.color;}
    public double getArea(){
        return 3.14*radius*radius;
    }
    @Override
    public String toString(){
        String s = " Radius = " + radius + " Color = " + color + " Area = " + getArea();
        return s;
    }

}

