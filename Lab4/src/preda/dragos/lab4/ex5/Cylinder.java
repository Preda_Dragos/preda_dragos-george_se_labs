package preda.dragos.lab4.ex5;

public class Cylinder extends Circle {
    private double height= 1.0;
    public Cylinder(){
    }

    public Cylinder(double radius) {
        super(radius);
    }
    public Cylinder(double radius,double height){
        super(radius); // or with this(radius)
        this.height = height;
    }
    public double getHeight() {
        return this.height;
    }
    public double getVolume(){
        double r = this.getRadius();
        double h = this.height;
        double pi = 3.1415;
        return pi*r*r*h;
    }
    @Override
    public double getArea(){
        double r = this.getRadius();
        double h = this.height;
        double pi = 3.1415;
        return 2*pi*r*r+h*2*pi*r;
    }
    @Override
    public String toString(){
        String s = "Radius = " +this.getRadius()+" Height = " + this.height + " Area = "+ this.getArea()+ " Volume = " + this.getVolume();
        return s;
    }

}
