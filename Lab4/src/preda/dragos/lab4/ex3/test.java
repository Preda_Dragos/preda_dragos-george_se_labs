package preda.dragos.lab4.ex3;

import preda.dragos.lab4.ex2.Author;

import java.util.Scanner;

public class test {
    public static void main(String args[]){
        String name,email,bookname;
        int price,qtyInStock;
        char gender;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Add a Book: ");
        bookname = scanner.next();
        System.out.print("Add its Author: Name: ");
        name = scanner.next();
        System.out.print(" Gender: ");
        gender = scanner.next().charAt(0);
        System.out.print(" Email : ");
        email = scanner.next();
        Author author = new Author(name,email,gender);
        System.out.print("Add its price($): ");
        price = scanner.nextInt();
        System.out.print("Available quantity in stock ");
        qtyInStock = scanner.nextInt();
        Book book = new Book(bookname,author,price);
        System.out.print(book.toString());
}
}
