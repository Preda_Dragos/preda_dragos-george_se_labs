package preda.dragos.lab4.ex1;
        import java.util.Scanner;
public class test {
    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Add a radius of a circle\n");
        int r = scanner.nextInt();
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(r);
        System.out.print("First circle: Radius= " + circle1.getRadius() );
        System.out.print("\nSecond circle: Radius= " + circle2.getRadius() );
        System.out.print("\nArea1 :" + circle1.getArea() +"\nArea2: " + circle2.getArea());
    }
}
