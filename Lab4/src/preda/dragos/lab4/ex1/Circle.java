package preda.dragos.lab4.ex1;

public class Circle {
    private double radius ;
    private String color ;
    public Circle() {
        this.radius = 1.0;
        this.color = "red";
    }
    public Circle(double radius){
        this.radius = radius;
        this.color  = "red";
        }
     public double getRadius(){
        return this.radius;
        }
    public double getArea(){
        return 3.14*radius*radius;
        }
    }

