package preda.dragos.lab4.ex6;

public class test {
    public static void main(String[] args){
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("black",false);
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(5);
        Circle circle3 = new Circle(5,"red",false);
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(7,8);
        Rectangle rectangle3 = new Rectangle(7,8,"yellow",true);
        Square square1 = new Square();
        Square square2 = new Square(5);
        Square square3 = new Square(5,"green",false);
        System.out.print("First shape: " + shape1.toString() + "\n");
        System.out.print("Second shape: " + shape2.toString() + "\n");
        System.out.print("First circle: " + circle1.toString() + "\n");
        System.out.print("Second circle: " + circle2.toString() + "\n");
        System.out.print("Third circle: " + circle3.toString() + "\n");
        System.out.print("First rectangle: " + rectangle1.toString() + "\n");
        System.out.print("Second rectangle: " + rectangle2.toString() + "\n");
        System.out.print("Third rectangle: " + rectangle3.toString() + "\n");
        System.out.print("First square: " + square1.toString() + "\n");
        System.out.print("Second square: " + square2.toString() + "\n");
        System.out.print("Third square: " + square3.toString() + "\n");
    }
}
