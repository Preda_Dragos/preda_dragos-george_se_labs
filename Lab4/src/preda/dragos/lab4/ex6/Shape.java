package preda.dragos.lab4.ex6;

import javax.swing.*;

public class Shape {
    private String color = "red";
    private boolean filled = true;
    public Shape(){
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled(){
        return this.filled;
    }

    public void setFilled(boolean filled){
        this.filled = filled;
    }
    @Override
    public String toString(){
        String s=" ";
        if(filled == true)
           s = "A Shape with color of " + this.color + " and filled ";
        else
            s = "A Shape with color of " + this.color + " and not filled ";
        return s;
    }
}
