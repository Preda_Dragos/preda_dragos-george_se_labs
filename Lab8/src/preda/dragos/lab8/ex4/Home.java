package preda.dragos.lab8.ex4;

import java.util.Random;

abstract class Home {
    AutomatedSystem automatedSystem = new AutomatedSystem();
    private Random r = new Random();
    private final int SIMULATION_STEPS = 20;
    private boolean smoke;
    private double temperature;

    protected abstract void setValueInEnvironment(Event event);
    protected abstract void controlStep();

    private Event getHomeEvent(){
        automatedSystem.createAutomatedSystem();
        //randomly generate a new event
        int k = r.nextInt(100);
        if(k<30)
            return new NoEvent();
        else if(k<60) {
            FireSensor fireSensor = automatedSystem.controlUnit.fireSensors.get(r.nextInt(automatedSystem.controlUnit.fireSensors.size()));
            FireEvent fireEvent = new FireEvent(r.nextBoolean(), fireSensor);
            smoke = fireEvent.isSmoke();
            fireSensor.readValue(smoke);
            return fireEvent;
        }
        else {
            TemperatureEvent temperatureEvent = new TemperatureEvent(r.nextInt(50));
            temperature = temperatureEvent.getValue();
            automatedSystem.controlUnit.temperatureSensor.readValue(temperature);
            return temperatureEvent;
        }
    }

    public void simulate(){
        int k = 0;
        while(k<SIMULATION_STEPS){
            Event event = this.getHomeEvent();
            setValueInEnvironment(event);
            automatedSystem.controlUnit.control(event);
            smoke = false;
            for(FireSensor fireSensor:automatedSystem.controlUnit.fireSensors){
                fireSensor.readValue(smoke);
            }
            controlStep();

            try{
                Thread.sleep(300);
            } catch(InterruptedException ex){
                ex.printStackTrace();
            }

            k++;
        }
    }
}
