package preda.dragos.lab8.ex4;

public class CoolingUnit {
    double temperature;
    double presetTemperature;

    public void cool(double temp, double presetTemp) {
        this.temperature = temp;
        this.presetTemperature = presetTemp;
        if (temperature > presetTemperature) {
            ControlUnit.save("Current temperature=" + temperature + "; goal temperature=" + presetTemperature);
            System.out.println("Current temperature=" + temperature + "; goal temperature=" + presetTemperature);
            ControlUnit.save("Cooling...");
            System.out.println("Cooling...");
            temperature = presetTemperature;
        } else if (temperature < presetTemperature)
            System.out.println("Error detected!");
        else {
            System.out.println("The temperature is equal to the preset temperature.");
            ControlUnit.save("The temperature is equal to the preset temperature.");
        }
    }
}
