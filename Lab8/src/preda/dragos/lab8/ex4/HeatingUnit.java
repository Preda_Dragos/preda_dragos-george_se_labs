package preda.dragos.lab8.ex4;

public class HeatingUnit {
    double temperature;
    double presetTemperature;

    public void heat(double temp, double presetTemp) {
        this.temperature = temp;
        this.presetTemperature = presetTemp;
        if (this.temperature < this.presetTemperature) {
            ControlUnit.save("Current temperature=" + this.temperature + "; goal temperature=" + this.presetTemperature);
            System.out.println("Current temperature=" + this.temperature + "; goal temperature=" + this.presetTemperature);
            ControlUnit.save("Heating...");
            System.out.println("Heating...");
            this.temperature = this.presetTemperature;
        } else if (this.temperature > this.presetTemperature)
            System.out.println("Error detected!");
        else {
            System.out.println("The temperature is equal to the preset temperature.");
            ControlUnit.save("The temperature is equal to the preset temperature.");
        }
    }
}
