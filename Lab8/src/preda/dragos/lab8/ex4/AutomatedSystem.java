package preda.dragos.lab8.ex4;

import java.util.*;

public class AutomatedSystem {
    TemperatureSensor temperatureSensor;
    ArrayList<FireSensor> fireSensors = new ArrayList<FireSensor>();
    AlarmUnit alarmUnit;
    HeatingUnit heatingUnit;
    CoolingUnit coolingUnit;
    ControlUnit controlUnit;

    public void createAutomatedSystem() {
        temperatureSensor = new TemperatureSensor("location_t");
        fireSensors.add(new FireSensor("location_f1"));
        fireSensors.add(new FireSensor("location_f2"));
        fireSensors.add(new FireSensor("location_f3"));
        fireSensors.add(new FireSensor("location_f4"));
        alarmUnit = new AlarmUnit();
        heatingUnit = new HeatingUnit();
        coolingUnit = new CoolingUnit();
        controlUnit=ControlUnit.getControlUnit(temperatureSensor,fireSensors,alarmUnit,heatingUnit,coolingUnit);
    }
}
