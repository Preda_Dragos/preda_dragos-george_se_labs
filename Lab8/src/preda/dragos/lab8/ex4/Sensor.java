package preda.dragos.lab8.ex4;

public class Sensor {
    private String location;

    public Sensor(String location){
        this.location = location;
    }

    public String getLocation(){
        return this.location;
    }
}
