package preda.dragos.lab5.ex4;

public class LightSensor extends Sensor {
    LightSensor(String location){
        super(location);
    }
    public int readValue(){
        return (int)(Math.random()*100);
    }
}
