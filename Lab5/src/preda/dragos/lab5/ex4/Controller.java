package preda.dragos.lab5.ex4;

public class Controller {
    private static Controller controller;
    public TemperatureSensor tempSensor;
    public LightSensor lightSensor;

    private Controller(TemperatureSensor tempSensor, LightSensor lightSensor) {
        this.tempSensor = tempSensor;
        this.lightSensor = lightSensor;
    }

    public static Controller getController() {
        if (controller == null) {
            TemperatureSensor tempSensor = new TemperatureSensor("loc1");
            LightSensor lightSensor = new LightSensor("loc2");
            controller = new Controller(tempSensor, lightSensor);
        }
        return controller;
    }

    public void control() {
        for (int i = 0; i < 20; i++) {
            int value_temp = tempSensor.readValue();
            int value_light = lightSensor.readValue();
            System.out.println("The temperature at second " + i + " is " + value_temp);
            System.out.println("The light at second " + i + " is " + value_light + "\n");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
