package preda.dragos.lab5.ex3;

public class Main {
    public static void main(String[] args) {
        Controller c = new Controller(new TemperatureSensor("loc1"), new LightSensor("loc2"));
        c.control();
    }
}
