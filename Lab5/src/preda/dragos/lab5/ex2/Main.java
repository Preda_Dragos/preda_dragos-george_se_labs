package preda.dragos.lab5.ex2;

public class Main {
    public static void main(String[] args) {
        ProxyImage proxyImage1 = new ProxyImage("abcd", "real");
        proxyImage1.display();
        ProxyImage proxyImage2 = new ProxyImage("xyzt", "rotated");
        proxyImage2.display();
    }
}
