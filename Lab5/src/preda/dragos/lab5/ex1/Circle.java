package preda.dragos.lab5.ex1;

public class Circle extends Shape{
    protected double radius = 1.0;
    public Circle(){
        this.radius = 1.0;
    }
    public Circle(double radius){
        this.radius = radius;
    }
    public Circle(double radius, String color, boolean filled){
        super(color, filled);
        this.radius = radius;

    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }
    @Override
    public double getArea(){
        return 3.14*radius*radius;
    }
    @Override
    public double getPerimeter(){
        return 2*3.14*radius;
    }

    @Override
    public String toString(){
        String s = "A circle with radius=" + radius + ", which is a subclass of " + super.toString();
        return  s;
    }
}
