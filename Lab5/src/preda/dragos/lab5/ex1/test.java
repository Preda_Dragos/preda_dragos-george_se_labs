package preda.dragos.lab5.ex1;

public class test {
    public static void main(String[] args){
        Shape[] shapes = new Shape[10];
        shapes[0] = new Rectangle();
        shapes[1] = new Rectangle(7,8);
        shapes[2] = new Rectangle(9,7,"green",true);
        shapes[3] = new Circle();
        shapes[4] = new Circle(3);
        shapes[5] = new Circle(4,"yellow",false);
        for( int i = 0; i < 10; i++)
        {
            System.out.print("Shape no" + (i+1) +".: Area=" + shapes[i].getArea() + " and Permineter=" + shapes[i].getPerimeter() + "\n");
        }
    }
}
