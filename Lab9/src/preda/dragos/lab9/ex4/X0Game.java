package preda.dragos.lab9.ex4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class X0Game extends JFrame {
    private String currentPlayer;
    JButton[][] buttons = new JButton[3][3];
    JButton newGame;
    JTextArea t1Area, t2Area;

    public X0Game(String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(350, 400);
        setVisible(true);
    }

    public void init() {
        currentPlayer = "X";
        setLayout(new GridLayout(4,3));
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++) {
                buttons[i][j] = new JButton();
                buttons[i][j].addActionListener(new ButtonListener());
                add(buttons[i][j]);
            }
        t1Area = new JTextArea();
        add(t1Area);
        t2Area = new JTextArea();
        add(t2Area);

        newGame = new JButton("New Game");
        newGame.addActionListener(new NewGame());
        add(newGame);
        pack();
    }

    public String getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean isBoardFull() {
        boolean isFull = true;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++) {
                if ((buttons[i][j].getText()).isBlank())
                    isFull = false;
            }
        return isFull;
    }

    public void changePlayer() {
        if (currentPlayer.equals("X")) {
            currentPlayer = "0";
        } else {
            currentPlayer = "X";
        }
    }

    public boolean checkForWin() {
        return (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin());
    }

    private boolean checkRowsForWin() {
        for (int i = 0; i < 3; i++) {
            if (checkRowCol(buttons[i][0].getText(), buttons[i][1].getText(), buttons[i][2].getText())) {
                return true;
            }
        }
        return false;
    }

    private boolean checkColumnsForWin() {
        for (int i = 0; i < 3; i++) {
            if (checkRowCol(buttons[0][i].getText(), buttons[1][i].getText(), buttons[2][i].getText())) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagonalsForWin() {
        boolean checkDiagonal1 = checkRowCol(buttons[0][0].getText(), buttons[1][1].getText(), buttons[2][2].getText());
        boolean checkDiagonal2 = checkRowCol(buttons[0][2].getText(), buttons[1][1].getText(), buttons[2][0].getText());
        return (checkDiagonal1 || checkDiagonal2);
    }

    // Check to see if three values are the same (and not empty) indicating a win.
    private boolean checkRowCol(String s1, String s2, String s3) {
        return ((!s1.isBlank()) && (s1.equals(s2)) && (s2.equals(s3)));
    }

    public void gameOver() {
        if (isBoardFull() && !checkForWin())
            t1Area.append("Game over!\nThe game was a tie.");
        else
            t1Area.append(getCurrentPlayer() + " wins!");
    }

    public static void main(String[] args) {
        new X0Game("X&0 Game!");
    }

    class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (((JButton) e.getSource()).getText().isBlank()) {
                ((JButton) e.getSource()).setText(currentPlayer);
            }
            if(!isBoardFull()&&!checkForWin())
                changePlayer();
            else
                gameOver();
        }

    }

    class NewGame implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            setVisible(false);
            dispose();
            new X0Game("X&0 Game!");
        }
    }
}
