package preda.dragos.lab9.ex3;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;

public class FileContent extends JFrame {
    JLabel fileName;
    JTextField tFileName;
    JTextArea contents;
    JButton button;

    FileContent() {
        setTitle("File Contents");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(350, 400);
        setVisible(true);
    }

    private void init() {
        this.setLayout(null);
        int width = 250;
        int height = 20;

        fileName = new JLabel("File Name ");
        fileName.setBounds(10, 50, width, height);

        tFileName = new JTextField();
        tFileName.setBounds(70, 50, width, height);

        button = new JButton("Display contents");
        button.setBounds(10, 150, width, height);

        button.addActionListener(new ButtonListener());

        contents = new JTextArea();
        contents.setBounds(10, 180, 300, 160);

        add(fileName);
        add(tFileName);
        add(button);
        add(contents);
    }

    public static void main(String[] args) {
        new FileContent();
    }

    class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String fileName = FileContent.this.tFileName.getText();
            BufferedReader in = null;
            try {
                in = new BufferedReader(new FileReader(fileName));
                String str;
                        while ((str = in.readLine()) != null)
                            FileContent.this.contents.append(str+"\n");

//                //another way of displaying the contents of the file
//                contents.read(in, " ");
//                contents.setLineWrap(true);
//                contents.setWrapStyleWord(true);

            } catch (IOException exception) {
                System.err.println(exception);
                System.exit(1);
            } finally {
                try {
                    in.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
