package preda.dragos.lab6.ex4;

import java.util.*;

public class Dictionary {
    HashMap<Word,Definition> dictionary = new HashMap<>();

    public void addWord(Word w, Definition d){
        if(dictionary.containsKey(w))
            System.out.println("Modifying existing word!");
        else System.out.println("Adding new word.");
        dictionary.put(w,d);
    }
    public Definition getDefinition(Word w){
        if (dictionary.containsKey(w))
            System.out.println("Word \""+w+"\" found");
        return dictionary.get(w);
    }
    public ArrayList<Word> getAllWords(){
        return new ArrayList<>(dictionary.keySet());
    }
    public ArrayList<Definition> getAllDefinitions(){
        return new ArrayList<>(dictionary.values());
    }
    public void displayDictionary(){
        System.out.println(dictionary);
    }
}
