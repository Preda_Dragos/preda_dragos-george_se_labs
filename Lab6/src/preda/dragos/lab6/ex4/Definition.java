package preda.dragos.lab6.ex4;

public class Definition {
    private String description;

    public Definition(String description){
        this.description = description;
    }

//    public String getDescription(){
//        return this.description;
//    }
    public String toString(){
        return this.description;
    }
}
