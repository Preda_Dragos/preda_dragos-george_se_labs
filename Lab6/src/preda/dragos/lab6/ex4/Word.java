package preda.dragos.lab6.ex4;

import java.util.Objects;

public class Word {
    private String name;

    public Word(String name){
        this.name = name;
    }

//    public String getName(){
//        return this.name;
//    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Word))
            return false;
        Word w = (Word) o;
        return name.equals(w.name);
    }

    @Override
    public int hashCode() {
        return name.length()*1000;
    }

    public String toString(){
        return this.name;
    }
}
