package preda.dragos.lab6.ex3;

import java.util.Comparator;

public class CompByOwner implements Comparator<BankAccount> {
    public int compare(BankAccount bA1, BankAccount bA2){
        return bA1.getOwner().compareToIgnoreCase(bA2.getOwner());
    }
}
