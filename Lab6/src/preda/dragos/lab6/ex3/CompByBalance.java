package preda.dragos.lab6.ex3;

import java.util.Comparator;

public class CompByBalance implements Comparator<BankAccount> {
    public int compare(BankAccount bA1, BankAccount bA2){
        if(bA1.getBalance() > bA2.getBalance())
            return 1;
        else return -1;
    }
}
