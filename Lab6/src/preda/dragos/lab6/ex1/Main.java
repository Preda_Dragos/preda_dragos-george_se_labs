package preda.dragos.lab6.ex1;

public class Main {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Pop Mihai", 23689.56);
        BankAccount b2 = new BankAccount("Mircea Diana", 500096.3);
        BankAccount b3 = new BankAccount("Pop Mihai", 23689.56);

        if(b1.equals(b2))
            System.out.println(b1+" and "+b2+" are equals.");
        else System.out.println(b1+" and "+b2+" are not equals.");

        if(b1.equals(b3))
            System.out.println(b1+" and "+b3+" are equals.");
        else System.out.println(b1+" and "+b3+" are not equals.");

        b1.withdraw(236.5);
        System.out.println(b1);
        b2.withdraw(1000000);
        b2.deposit(10000);
        System.out.println(b2);
    }
}
