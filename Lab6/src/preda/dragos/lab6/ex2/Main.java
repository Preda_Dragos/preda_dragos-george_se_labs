package preda.dragos.lab6.ex2;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Bank b = new Bank();
        b.addAccount("Ana", 3000);
        b.addAccount("Irina", 256.24);
        b.addAccount("Maria", 5999.99);
        b.addAccount("Costel", 6662478.3);
        b.printAccounts();
        System.out.println("\n");
        b.printAccounts(600, 6000);

        BankAccount acc = b.getAccount("Ana");
        if (acc != null)
            System.out.println("\nThe account(s) owned by Ana: " + acc);
        else System.out.println("\nThere is no account owned by Ana.");

        BankAccount acc1 = b.getAccount("Larisa");
        if (acc1 != null)
            System.out.println("\nThe account(s) owned by Larisa: " + acc1);
        else System.out.println("\nThere is no account owned by Larisa.");

        Comparator<BankAccount> compareByOwner = (BankAccount o1, BankAccount o2) -> o1.getOwner().compareToIgnoreCase(o2.getOwner());
        ArrayList<BankAccount> allAccounts = b.getAllAccounts();
        Collections.sort(allAccounts, compareByOwner);
        System.out.println("\nThe accounts sorted alphabetically by owner: ");
        System.out.println(allAccounts);
    }
}
