package preda.dragos.lab6.ex2;

import java.util.*;

public class Bank {
    private ArrayList<BankAccount> bankAccounts = new ArrayList<>();

    public void addAccount(String owner, double balance) {
        BankAccount bA = new BankAccount(owner, balance);
        bankAccounts.add(bA);
    }

    public void printAccounts() {
        System.out.println("The accounts sorted by balance: ");
        Collections.sort(bankAccounts);
        System.out.println(bankAccounts);
    }

    public void printAccounts(double minBalance, double maxBalance) {
        Collections.sort(bankAccounts);
        Iterator it = bankAccounts.iterator();
        System.out.println("The accounts with the balance between "+minBalance+" and "+maxBalance+": ");
        while (it.hasNext()) {
            BankAccount bA = (BankAccount) it.next();
            if (bA.getBalance() >= minBalance && bA.getBalance() <= maxBalance)
                System.out.println(bA);
        }
    }

    public BankAccount getAccount(String owner){
        boolean check;
        BankAccount acc = null;
        Iterator it = bankAccounts.iterator();
        while (it.hasNext()) {
            BankAccount bA = (BankAccount) it.next();
            check = bA.getOwner().equalsIgnoreCase(owner);
            if(check)
                acc = bA;
        }
        return acc;
    }

    public ArrayList<BankAccount> getAllAccounts(){
        return this.bankAccounts;
    }
}
